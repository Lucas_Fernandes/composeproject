package com.repository.composeproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainScreen()
        }
    }
}


@Composable
fun MainScreen() {
    //CREATING A DARK BACKGROUND FILLING THE WHOLE SCREEN.
    Surface(
        color = Color.DarkGray,
        modifier = Modifier.fillMaxSize()
    ) {

//        Row(
//            modifier = Modifier.fillMaxSize(),
//            horizontalArrangement = Arrangement.SpaceAround,
//            verticalAlignment = Alignment.CenterVertically
//        ) {
//            HorizontalColoredBar(Color.Red)
//            HorizontalColoredBar(Color.Magenta)
//            HorizontalColoredBar(Color.Cyan)
//            HorizontalColoredBar(Color.Yellow)
//            HorizontalColoredBar(Color.Blue)
//        }

//        Column(
//            modifier = Modifier.fillMaxSize(),
//            verticalArrangement = Arrangement.SpaceEvenly,
//            horizontalAlignment = Alignment.CenterHorizontally
//        ) {
//            VerticalColoredBar(Color.Red)
//            VerticalColoredBar(Color.Magenta)
//            VerticalColoredBar(Color.Cyan)
//            VerticalColoredBar(Color.Yellow)
//            VerticalColoredBar(Color.Blue)
//        }

        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                ColoredSquare(Color.Yellow)
                ColoredSquare(Color.Blue)
            }

            VerticalColoredBar(Color.Red)
            VerticalColoredBar(Color.Magenta)
            VerticalColoredBar(Color.Cyan)
            VerticalColoredBar(Color.Yellow)
            VerticalColoredBar(Color.Blue)
        }

    }
}

@Composable
fun HorizontalColoredBar(color: Color) {
    Surface(
        color = color,
        modifier = Modifier
            .height(600.dp)
            .width(60.dp)
    ) { }
}

@Composable
fun VerticalColoredBar(color: Color) {
    Surface(
        color = color,
        modifier = Modifier
            .height(100.dp)
            .width(350.dp)
    ) { }

}

@Composable
fun ColoredSquare(color: Color) {
    Surface(
        color = color,
        modifier = Modifier
            .height(100.dp)
            .width(100.dp)
    ) {

    }
}

@Composable
fun GreetingButton() {
    Button(onClick = { }) {
        GreetingText(name = "button")
        GreetingText(name = "remaining button")
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreviewMainActivity() {
    MainScreen()
}