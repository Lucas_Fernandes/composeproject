package com.repository.composeproject

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun GreetingText(name: String) {
    Text(
        text = "Hello $name!",
        modifier = Modifier
            .size(height = 240.dp, width = 200.dp)
            .clickable(onClick = {})
            .padding(all = 24.dp),
        style = MaterialTheme.typography.h5,
        fontWeight = FontWeight.SemiBold
    )
}


